import { component, React, Style, Children } from 'local/react/component'
import { Icon } from 'local/react-ui/icons'

export const defaultBreadcrumbStyle: Style = {
  display: 'inline-block'
}

export const defaultBreadcrumbIconStyle: Style = {
  margin: '0 0.5em',
  opacity: 0.5
}

export const Breadcrumb = component.children
  .props<{
    className?: string
    children: Children
    style?: Style
    iconStyle?: Style
    iconName?: string
  }>({
    style: defaultBreadcrumbStyle,
    iconName: 'angle-right',
    iconStyle: defaultBreadcrumbIconStyle
  })
  .render(({ className, children, style, iconName, iconStyle }) => (
    <div style={style} className={className}>
      <Icon name={iconName} style={iconStyle} />
      {children}
    </div>
  ))
